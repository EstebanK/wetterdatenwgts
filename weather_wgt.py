"""
SERVICES:
- feiertage-api.de

- DWD (URL?)
- Messnetzkarte https://www.dwd.de/DE/derdwd/messnetz/bodenbeobachtung/messnetzkarte_boden.pdf?__blob=publicationFile&v=9
- Stationsübersicht https://opendata.dwd.de/climate_environment/CDC/help/stations_list_CLIMAT_data.txt
- sonnenverlauf
"""

import time
import datetime
import json

import requests

import plotly.graph_objects as go
from plotly.subplots import make_subplots

import pandas as pd

pd.options.plotting.backend = 'plotly'

STATION = '02928'  # Leipzig-Holzhausen

years = range(2007, 2023)
climate_file = 'wetter_leipzig.csv'
temp_file = 'temp_hourly_leipzig.csv'
precip_file = None


def date_getter(jahr):
    url = f'https://feiertage-api.de/api/?jahr={jahr}'
    resp = requests.get(url)
    con = resp.content
    return json.loads(con)['BB']['Pfingstsonntag']['datum']


def df_reader(file):
    frame = pd.read_csv(file, sep=';')
    try:
        frame['MESS_DATUM'] = pd.to_datetime(
            frame['MESS_DATUM'], format='%Y%m%d%H')
        frame['HOUR'] = frame['MESS_DATUM'].dt.hour
    except ValueError:
        frame['MESS_DATUM'] = pd.to_datetime(
            frame['MESS_DATUM'], format='%Y%m%d')
    frame['YEAR'] = frame['MESS_DATUM'].dt.year
    frame['MONTH'] = frame['MESS_DATUM'].dt.month
    frame['DAY'] = frame['MESS_DATUM'].dt.day
    frame.reset_index(drop=True, inplace=True)
    frame.set_index('MESS_DATUM', drop=True, inplace=True)
    return frame


def get_wgt_date(pentecote, diff):
    begin = pd.to_datetime(pentecote, format='%Y-%m-%d') + \
        datetime.timedelta(diff)
    return begin.strftime('%Y-%m-%d')


weather_frame = df_reader(climate_file)

temp_frame = df_reader(temp_file)

#precip_frame = df_reader(precip_file)
#precip_frame.head()

dates = []
for year in years:
    pentecost = date_getter(year)
    dates.append(pentecost)
    time.sleep(1)
    print(f'{year} done.')


dateranges = []
for date in dates:
    start = get_wgt_date(date, -2)
    end = get_wgt_date(date, 2)
    dateranges += pd.date_range(start=start, end=end).tolist()

hourranges = []
for date in dates:
    start = get_wgt_date(date, -2)
    end = get_wgt_date(date, 2)
    hourranges += pd.date_range(start=start, end=end, freq='H').tolist()


weather_frame = weather_frame.loc[weather_frame.index.isin(dateranges)]


fig = None
fig = make_subplots(rows=2, cols=1, shared_xaxes=True,
                    subplot_titles=("temperature in °C", "precipitation in mm"))

fig.append_trace(
    go.Box(x=weather_frame['YEAR'], y=weather_frame[' TMK'],), row=1, col=1)
fig.append_trace(
    go.Box(x=weather_frame['YEAR'], y=weather_frame[' RSK'],), row=2, col=1)
fig.update_layout(showlegend=False,
                  title_text="Weather in WGTs", template='plotly_dark')
fig.write_html('index.html')
